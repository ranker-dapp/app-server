import { Injectable, OnModuleInit } from '@nestjs/common';
import { Murmuration } from './entities/murmuration.entity';
import { UsersService } from 'src/users/users.service';
import { ProjectionsService } from 'src/projections/projections.service';

@Injectable()
export class MurmurationsService implements OnModuleInit {
  private murmurations: Murmuration[] = [];

  constructor(private usersService: UsersService, private projectionsService: ProjectionsService) {
    this.murmurations = [{
      name: 'Summer Trip',
      users: [],
    }]
  }

  async onModuleInit () {
    const users = await this.usersService.findAll()
    this.murmurations[0].users = users.splice(1, 2)

    const projections = await this.projectionsService.findAll()
    this.murmurations[0].projection = projections[0]
  }

  create (murmuration: Murmuration) {
    this.murmurations.push(murmuration);
    return murmuration;
  }

  findAll () {
    return this.murmurations;
  }

  findOne (name: string) {
    const murmuration = this.murmurations.find(murmuration => murmuration.name === name);
    if (murmuration) {
      return murmuration;
    }
    return null;
  }

  update(currentName: string, murmuration: Murmuration) {
    let savedMurmuration = this.murmurations.find(murmuration => murmuration.name === currentName);
    if (savedMurmuration) {
      savedMurmuration = murmuration;
      return savedMurmuration;
    }
    return null;
  }

  remove (name: string) {
    const murmuration = this.murmurations.find(murmuration => murmuration.name === name);
    if (murmuration) {
      this.murmurations.splice(this.murmurations.indexOf(murmuration), 1);
    }
    return murmuration;
  }
}
