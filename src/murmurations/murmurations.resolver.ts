import { Resolver, Query, Mutation, Args, ResolveField, Parent } from '@nestjs/graphql';
import { MurmurationsService } from './murmurations.service';
import { Murmuration } from './entities/murmuration.entity';
import { CreateMurmurationInput } from './dto/create-murmuration.input';
import { UpdateMurmurationInput } from './dto/update-murmuration.input';
import { ProjectionsService } from 'src/projections/projections.service';

@Resolver(() => Murmuration)
export class MurmurationsResolver {
  constructor(
    private readonly murmurationsService: MurmurationsService,
    private readonly projectionsService: ProjectionsService
  ) { }

  @Mutation(() => Murmuration)
  createMurmuration (@Args('createMurmurationInput') createMurmurationInput: CreateMurmurationInput) {
    return this.murmurationsService.create(createMurmurationInput);
  }

  @Query(() => [Murmuration], { name: 'murmurations' })
  findAll () {
    return this.murmurationsService.findAll();
  }

  @ResolveField()
  async projection (@Parent() murmuration: Murmuration) {
    return this.projectionsService.computeProjection(murmuration);
  }

  @Query(() => Murmuration, { name: 'murmuration' })
  findOne (@Args('name', { type: () => String }) name: string) {
    return this.murmurationsService.findOne(name);
  }

  @Mutation(() => Murmuration)
  updateMurmuration (@Args('updateMurmurationInput') { currentName, ...murmuration }: UpdateMurmurationInput) {
    return this.murmurationsService.update(currentName as string, murmuration as Murmuration);
  }

  @Mutation(() => Murmuration)
  removeMurmuration (@Args('name', { type: () => String }) name: string) {
    return this.murmurationsService.remove(name);
  }
}
