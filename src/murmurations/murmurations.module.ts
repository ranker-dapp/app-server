import { Module } from '@nestjs/common';
import { MurmurationsService } from './murmurations.service';
import { MurmurationsResolver } from './murmurations.resolver';
import { UsersModule } from 'src/users/users.module';
import { ProjectionsModule } from 'src/projections/projections.module';

@Module({
  imports: [UsersModule, ProjectionsModule],
  providers: [MurmurationsResolver, MurmurationsService],
  exports: [MurmurationsService]
})
export class MurmurationsModule {}
