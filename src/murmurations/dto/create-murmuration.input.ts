import { InputType, Field } from '@nestjs/graphql';
import { Projection } from 'src/projections/entities/projection.entity';
import { User } from 'src/users/entities/user.entity';

@InputType()
export class CreateMurmurationInput {
  @Field(() => String, {description: 'name' })
  name!: String;

  @Field(() => [User], { description: 'users' })
  users!: User[];

  @Field(() => Projection, { description: 'projection' })
  projection!: Projection;
}
