import { User } from 'src/users/entities/user.entity';
import { CreateMurmurationInput } from './create-murmuration.input';
import { InputType, Field, PartialType } from '@nestjs/graphql';
import { Projection } from 'src/projections/entities/projection.entity';

@InputType()
export class UpdateMurmurationInput extends PartialType(CreateMurmurationInput) {
  @Field(() => String, {description: 'current name' })
  currentName!: String;

  @Field(() => String, {description: 'new name' })
  newName!: String;

  @Field(() => [User], { description: 'users' })
  users!: User[];

  @Field(() => Projection, { description: 'projection' })
  projection!: Projection;
}
