import { ObjectType, Field } from '@nestjs/graphql';
import { Projection } from 'src/projections/entities/projection.entity';
import { User } from 'src/users/entities/user.entity';

@ObjectType()
export class Murmuration {
  @Field(() => String, {description: 'name' })
  name!: String;

  @Field(() => [User], { description: 'users' })
  users?: User[];

  @Field(() => Projection, { description: 'projection' })
  projection?: Projection;
}
