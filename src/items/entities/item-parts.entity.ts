import { ObjectType, Field } from '@nestjs/graphql';
import { User } from 'src/users/entities/user.entity';

@ObjectType()
export class ItemParts {
  @Field(() => String, { description: 'title' })
  title!: string;

  @Field(() => String, { description: 'comment' })
  comment!: string;

  @Field(() => User, { description: 'author' })
  author!: User;
}
