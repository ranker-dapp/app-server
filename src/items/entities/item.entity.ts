import { ObjectType, Field, Int, InputType } from '@nestjs/graphql';
import { Dimension } from 'src/dimensions/entities/dimension.entity';
import { User } from 'src/users/entities/user.entity';

@InputType('ItemInput')
@ObjectType()
export class Item {
  @Field(() => Int, { description: 'id' })
  id!: number;

  @Field(() => String, { description: 'title' })
  title!: string;

  @Field(() => String, { description: 'comment' })
  comment!: string;

  @Field(() => String, { description: 'comment date' })
  commentDate!: string;

  @Field(() => User, { description: 'author' })
  author?: User;

  @Field(() => [Dimension], { description: 'dimensions' })
  dimensions?: Dimension[];

  @Field(() => [User], { description: 'author' })
  users?: User[];
}
