import { Module, forwardRef } from '@nestjs/common';
import { ItemsService } from './items.service';
import { ItemsResolver } from './items.resolver';
import { UsersModule } from 'src/users/users.module';
import { DimensionsModule } from 'src/dimensions/dimensions.module';

@Module({
  imports: [forwardRef(() => UsersModule), forwardRef(() => DimensionsModule)],
  providers: [ItemsResolver, ItemsService],
  exports: [ItemsService]
})
export class ItemsModule {}
