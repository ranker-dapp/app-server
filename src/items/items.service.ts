import { Injectable, OnModuleInit } from '@nestjs/common';
import { UpdateItemInput } from './dto/update-item.input';
import { Item } from './entities/item.entity';
import { ItemParts } from './entities/item-parts.entity';

@Injectable()
export class ItemsService implements OnModuleInit {
  private items: Item[] = [];

  constructor() {
    this.items = [{
      id: 1,
      title: 'Paris',
      comment: 'City of lights',
      commentDate: (new Date()).toISOString(),
    }, {
      id: 2,
      title: 'Milan',
      comment: 'Capital of fashion',
      commentDate: (new Date()).toISOString(),
    }, {
      id: 2,
      title: 'Amsterdam',
      comment: 'Weed up',
      commentDate: (new Date()).toISOString(),
    }];
  }

  async onModuleInit () {
    // for (const item of this.items) {
    //   item.author = await this.usersService.findOne('admin')
    // }
  }

  create (itemParts: ItemParts) {
    const item = {
      ...itemParts,
      id: this.items.length + 1,
      commentDate: (new Date()).toISOString(),
    }
    this.items.push(item);
    return item;
  }

  findAll () {
    return this.items;
  }

  async findOne (id: number) {
    const item = this.items.find(item => item.id === id);
    if (item) {
      return item;
    }
    return null;
  }

  update (id: number, updateItemInput: UpdateItemInput) {
    const item = this.items.find(item => item.id === id);
    if (item) {
      item.title = updateItemInput.title;
      item.comment = updateItemInput.comment;
      item.commentDate = (new Date()).toISOString();
    }
    return item;
  }

  remove (id: number) {
    const item = this.items.find(item => item.id === id);
    if (item) {
      this.items.splice(this.items.indexOf(item), 1);
    }
    return item;
  }
}
