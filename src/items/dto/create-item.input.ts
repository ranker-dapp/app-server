import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class CreateItemInput {
  @Field(() => String, { description: 'title' })
  title!: string;

  @Field(() => String, { description: 'comment' })
  comment!: string;
}
