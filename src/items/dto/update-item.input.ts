import { CreateItemInput } from './create-item.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateItemInput extends PartialType(CreateItemInput) {
  @Field(() => Int)
  id!: number;

  @Field(() => String, { description: 'title' })
  title!: string;

  @Field(() => String, { description: 'comment' })
  comment!: string;
}
