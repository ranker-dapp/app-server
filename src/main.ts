import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Configurer } from '@deposition.cloud/conf';

const conf = new Configurer({
  packageLockJsonFilePath: 'package-lock.json',
  required: ['port'],
  files: process.env.NODE_ENV === 'production' ? ['config.yml']: ['config.dev.yml', 'config.yml']
});

const port = conf.get('port');

console.log(`Server will be listening on port: ${port}`)

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const allowedOrigins = [
    'https://studio.apollographql.com',
    'https://preflight-request.apollographql.com',
  ];

  app.enableCors({
    origin: (origin, callback) => {
      if (!origin) return callback(null, true);

      if (allowedOrigins.includes(origin)) {
        return callback(null, true);
      }

      if (
        /^http:\/\/localhost(:\d+)?$/.test(origin) ||
        /^http:\/\/192\.168\.1\./.test(origin)
      ) {
        return callback(null, true);
      }

      callback(new Error('Not allowed by CORS'));
    },
    credentials: true,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    allowedHeaders: 'Content-Type, Accept, Authorization',
  });

  await app.listen(port);
}

bootstrap();
