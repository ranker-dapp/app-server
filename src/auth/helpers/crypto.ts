import { scrypt, randomBytes, timingSafeEqual } from "crypto";
import { promisify } from 'util';

const keyLength = 64; // Increased key length

const scryptAsync = promisify(scrypt); // Promisify scrypt to use async/await

/**
 * Has a password or a secret with a password hashing algorithm (scrypt)
 * @param {string} password
 * @returns {string} The salt+hash
 */
export const hash = async (password: string) => {
  const salt = randomBytes(16).toString("hex");
  const derivedKey: any = await scryptAsync(password, salt, keyLength);
  return `${salt}.${derivedKey.toString("hex")}`;
};

/**
 * Compare a plain text password with a salt+hash password
 * @param {string} password The plain text password
 * @param {string} hash The hash+salt to check against
 * @returns {boolean}
 */
export const compare = async (password: string, hash: string) => {
  const [salt, hashKey] = hash.split(".");
  const hashKeyBuff = Buffer.from(hashKey, "hex");
  const derivedKey: any = await scryptAsync(password, salt, keyLength);
  return timingSafeEqual(hashKeyBuff, derivedKey);
};