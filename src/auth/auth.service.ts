import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { User } from 'src/users/entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import { LoginResponse } from './dto/login-response';
import { CreateUserInput } from 'src/users/dto/create-user.input';
import { compare } from './helpers/crypto';

@Injectable()
export class AuthService {
  constructor(private usersService: UsersService, private jwtService: JwtService) { }

  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.usersService.findOne(username);

    if (user && await compare(password, user.passwordHash)) {
      const { passwordHash, ...result } = user;
      return result;
    }
    return null;
  }

  login (user: User) {
    const payload = { username: user.username, sub: user.username };
    const accessToken = this.jwtService.sign(payload)
    const loginResponse: LoginResponse = {
      accessToken,
      user
    }
    return loginResponse
  }

  signup (user: CreateUserInput) {
    return this.usersService.create(user)
  }
}
