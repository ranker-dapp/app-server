import { ObjectType, Field, InputType } from '@nestjs/graphql';
import { Item } from 'src/items/entities/item.entity';
import { User } from 'src/users/entities/user.entity';

@InputType('DimensionInput')
@ObjectType()
export class Dimension {
  @Field(() => String, { description: 'name' })
  name!: string;

  @Field(() => [User], { description: 'users' })
  users?: User[];

  @Field(() => [Item], { description: 'items' })
  items?: Item[];
}
