import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { DimensionsService } from './dimensions.service';
import { Dimension } from './entities/dimension.entity';
import { CreateDimensionInput } from './dto/create-dimension.input';
import { UpdateDimensionInput } from './dto/update-dimension.input';

@Resolver(() => Dimension)
export class DimensionsResolver {
  constructor(private readonly dimensionsService: DimensionsService) {}

  @Mutation(() => Dimension)
  createDimension(@Args('createDimensionInput') createDimensionInput: CreateDimensionInput) {
    return this.dimensionsService.create(createDimensionInput);
  }

  @Query(() => [Dimension], { name: 'dimensions' })
  findAll() {
    return this.dimensionsService.findAll();
  }

  @Query(() => Dimension, { name: 'dimension' })
  findOne(@Args('name', { type: () => String }) name: string) {
    return this.dimensionsService.findOne(name);
  }

  @Mutation(() => Dimension)
  updateDimension(@Args('updateDimensionInput') updateDimensionInput: UpdateDimensionInput) {
    return this.dimensionsService.update(updateDimensionInput);
  }

  @Mutation(() => Dimension)
  removeDimension(@Args('name', { type: () => String }) name: string) {
    return this.dimensionsService.remove(name);
  }
}
