import { Injectable } from '@nestjs/common';
import { UpdateDimensionInput } from './dto/update-dimension.input';
import { Dimension } from './entities/dimension.entity';

@Injectable()
export class DimensionsService {
  private dimensions: Dimension[] = [];

  constructor() {
    this.dimensions = [
      { name: 'Fun' },
      { name: 'Expensive' },
      { name: 'Civilized' }
    ];
  }

  async onModuleInit () {}

  create (dimension: Dimension) {
    this.dimensions.push(dimension);
    return dimension;
  }

  findAll () {
    return this.dimensions;
  }

  findOne (name: string) {
    const dimension = this.dimensions.find(dimension => dimension.name === name);
    if (dimension) {
      return dimension;
    }
    return null;
  }

  update (updateDimensionInput: UpdateDimensionInput) {
    const dimension = this.dimensions.find(dimension => dimension.name === updateDimensionInput.currentName);
    if (dimension) {
      dimension.name = updateDimensionInput.newName;
    }
    return dimension;
  }

  remove (name: string) {
    const dimension = this.dimensions.find(dimension => dimension.name === name);
    if (dimension) {
      this.dimensions.splice(this.dimensions.indexOf(dimension), 1);
    }
    return dimension;
  }
}
