import { Module, forwardRef } from '@nestjs/common';
import { DimensionsService } from './dimensions.service';
import { DimensionsResolver } from './dimensions.resolver';
import { ItemsModule } from 'src/items/items.module';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [forwardRef(() => UsersModule), forwardRef(() => ItemsModule)],
  providers: [DimensionsResolver, DimensionsService],
  exports: [DimensionsService],
})
export class DimensionsModule {}
