import { CreateDimensionInput } from './create-dimension.input';
import { InputType, Field, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateDimensionInput extends PartialType(CreateDimensionInput) {
  @Field(() => String, { description: 'current name' })
  currentName!: string;

  @Field(() => String, { description: 'new name' })
  newName!: string;
}
