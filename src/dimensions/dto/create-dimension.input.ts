import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class CreateDimensionInput {
  @Field(() => String, { description: 'name' })
  name!: string;
}
