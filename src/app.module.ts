import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { UsersModule } from './users/users.module';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { AuthService } from './auth/auth.service';
import { AuthResolver } from './auth/auth.resolver';
import { AuthModule } from './auth/auth.module';
import { ItemsModule } from './items/items.module';
import { DimensionsModule } from './dimensions/dimensions.module';
import { MurmurationsModule } from './murmurations/murmurations.module';
import { ProjectionsModule } from './projections/projections.module';

@Module({
  imports: [
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: 'schema.gql',
      sortSchema: true,
      installSubscriptionHandlers: true,
      introspection: true,
      subscriptions: {
        'graphql-ws': true
      },
    }),
    UsersModule,
    AuthModule,
    ItemsModule,
    DimensionsModule,
    MurmurationsModule,
    ProjectionsModule,
  ],
  providers: [AuthService, AuthResolver],
})

export class AppModule {}
