import { Injectable } from '@nestjs/common';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { SavedUser } from './entities/saved-user.entity';
import { hash } from '../auth/helpers/crypto';
import { DimensionsService } from 'src/dimensions/dimensions.service';
import { ItemsService } from 'src/items/items.service';

@Injectable()
export class UsersService {
  private users: SavedUser[] = [];

  constructor(private dimensionsService: DimensionsService, private itemsService: ItemsService) {
    this.users = [{
      username: 'admin',
      passwordHash: '580342459c833c8bd5d60c2b6ffc8590.03f762132dd4ec199dcc28336f80bec5bf142394e9a0756339b91a20d9d3f754091a23a29754644ec6071e52c37ab53fc99d9409098ea1398835f14ad9fb54ee',
      admin: true,
    }, {
      username: 'pcuci',
      passwordHash: '580342459c833c8bd5d60c2b6ffc8590.03f762132dd4ec199dcc28336f80bec5bf142394e9a0756339b91a20d9d3f754091a23a29754644ec6071e52c37ab53fc99d9409098ea1398835f14ad9fb54ee',
    }, {
      username: '3mblay',
      passwordHash: '580342459c833c8bd5d60c2b6ffc8590.03f762132dd4ec199dcc28336f80bec5bf142394e9a0756339b91a20d9d3f754091a23a29754644ec6071e52c37ab53fc99d9409098ea1398835f14ad9fb54ee',
    }];
  }

  async onModuleInit () {
    const dimensions = await this.dimensionsService.findAll()

    for (const user of this.users) {
      user.dimensions = [...dimensions].sort(() => Math.random() - 0.5);
    }

    const items = await this.itemsService.findAll();

    for (const user of this.users) {
      if (user.dimensions) {
        for (const dimension of user.dimensions) {
          dimension.items = [...items].sort(() => Math.random() - 0.5);
        }
      }
    }
  }

  async create (createUserInput: CreateUserInput) {
    if (this.users.find(user => user.username === createUserInput.username)) {
      throw new Error('User already exists');
    }

    const passwordHash = await hash(createUserInput.password)

    const user = {
      ...createUserInput,
      passwordHash,
    }

    this.users.push(user);

    return user;
  }

  async findAll () {
    return this.users;
  }

  async findOne (username: string): Promise<SavedUser | undefined> {
    return this.users.find(user => user.username === username);
  }

  update (updateUserInput: UpdateUserInput) {
    const user = {
      ...updateUserInput,
    }

    this.users.find(u => u.username === user.username)

    return user;
  }
}
