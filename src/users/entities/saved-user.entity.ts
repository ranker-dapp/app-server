import { ObjectType, Field } from '@nestjs/graphql';
import { Dimension } from 'src/dimensions/entities/dimension.entity';

@ObjectType()
export class SavedUser {
  @Field(() => String, { description: 'username' })
  username!: string;

  @Field(() => String, { description: 'password' })
  passwordHash!: string;

  @Field(() => Boolean, { description: 'admin' })
  admin?: boolean;

  @Field(() => [Dimension], { description: 'dimensions' })
  dimensions?: Dimension[];
}
