import { ObjectType, Field, InputType } from '@nestjs/graphql';
import { Dimension } from 'src/dimensions/entities/dimension.entity';
import { Item } from 'src/items/entities/item.entity';

@InputType('UserInput')
@ObjectType()
export class User {
  @Field(() => String, { description: 'username' })
  username!: string;

  @Field(() => [Dimension], { description: 'dimensions' })
  dimensions?: Dimension[];

  @Field(() => [Item], { description: 'items' })
  items?: Item[];
}
