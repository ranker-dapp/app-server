import { Module, forwardRef } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersResolver } from './users.resolver';
import { DimensionsModule } from 'src/dimensions/dimensions.module';
import { ItemsModule } from 'src/items/items.module';

@Module({
  imports: [forwardRef(() => DimensionsModule), forwardRef(() => ItemsModule)],
  providers: [UsersResolver, UsersService],
  exports: [UsersService],
})
export class UsersModule {}
