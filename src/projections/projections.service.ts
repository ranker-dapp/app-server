import { Injectable } from '@nestjs/common';
import { Projection } from './entities/projection.entity';
import { DimensionsService } from 'src/dimensions/dimensions.service';
import { Murmuration } from 'src/murmurations/entities/murmuration.entity';
import { Dimension } from 'src/dimensions/entities/dimension.entity';
import { averageRank } from 'src/projections/helpers/average-rank';

@Injectable()
export class ProjectionsService {
  private projections: Projection[] = [];

  constructor(
    private dimensionsService: DimensionsService,
  ) {
    this.projections = [{
      name: 'expensive fun',
      dimensions: [{
        name: 'Expensive'
      }, {
        name: 'Fun'
      }]
    }, {
      name: 'fancy comfort',
      dimensions: [{
        name: 'Expensive'
      }, {
        name: 'Civilized'
      }],
    }]
  }

  async onModuleInit () {
    this.projections[0].dimensions = (await this.dimensionsService.findAll()).splice(1, 2)
    this.projections[1].dimensions = (await this.dimensionsService.findAll()).splice(0, 2)
  }

  create (projection: Projection) {
    this.projections.push(projection);
    return projection;
  }

  findAll () {
    return this.projections;
  }

  computeProjection (murmuration: Murmuration) {
    const murmurationDimensions: Dimension[] = [];
    const { users, projection } = murmuration;
    if (projection && projection.dimensions) {
      if (users) {
        for (const user of users) {
          if (user.dimensions) {
            murmurationDimensions.push(averageRank(user.dimensions));
          }
        }
      }
    }

    return {
      ...murmuration.projection,
      dimension: averageRank(murmurationDimensions)
    };
  }

  findOne (name: string) {
    const projection = this.projections.find(projection => projection.name === name);
    if (projection) {
      return projection;
    }
    return projection;
  }

  update (currentName: string, projection: Projection) {
    let savedProjection = this.projections.find(projection => projection.name === currentName);
    if (savedProjection) {
      savedProjection = projection;
      return savedProjection;
    }
    return null;
  }

  remove (name: string) {
    const projection = this.projections.find(projection => projection.name === name);
    if (projection) {
      this.projections.splice(this.projections.indexOf(projection), 1);
    }
    return projection;
  }
}
