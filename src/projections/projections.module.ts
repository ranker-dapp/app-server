import { Module } from '@nestjs/common';
import { ProjectionsService } from './projections.service';
import { ProjectionsResolver } from './projections.resolver';
import { DimensionsModule } from 'src/dimensions/dimensions.module';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [DimensionsModule, UsersModule],
  providers: [ProjectionsResolver, ProjectionsService],
  exports: [ProjectionsService]
})
export class ProjectionsModule {}
