import { Dimension } from "src/dimensions/entities/dimension.entity";

export function averageRank (dimensions: Dimension[]): Dimension {
  let rankSums = new Map<string, number>();
  let commentSums = new Map<string, string>();
  let itemCount = new Set(dimensions.flatMap(dimension => dimension.items).map(item => item?.title)).size;
  let dimensionNames = dimensions.map(dimension => dimension.name);

  for (let dimension of dimensions) {
    let seenItems = new Set();
    if (dimension.items) {
      dimension.items.forEach((item, index) => {
        rankSums.set(item.title, (rankSums.get(item.title) || 0) + index + 1);
        commentSums.set(item.title, (commentSums.get(item.title) || "") + " " + item.comment);
        seenItems.add(item.title);
      });
    }

    for (let itemTitle of rankSums.keys()) {
      if (!seenItems.has(itemTitle)) {
        rankSums.set(itemTitle, (rankSums.get(itemTitle) || 0) + itemCount);
      }
    }
  }

  let averageRanks = Array.from(rankSums.entries()).map(([title, rankSum]) => ({
    title,
    comment: commentSums.get(title) || "",
    averageRank: rankSum / dimensions.length
  }));

  averageRanks.sort((a, b) => a.averageRank - b.averageRank);

  let mergedItems = averageRanks.map(rankObj => ({
    id: rankObj.averageRank,
    title: rankObj.title,
    comment: rankObj.comment.trim(),
    commentDate: new Date().toISOString()
  }));

  return {
    name: dimensionNames.join('-'),
    items: mergedItems
  };
}