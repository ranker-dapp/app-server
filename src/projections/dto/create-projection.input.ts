import { InputType, Field } from '@nestjs/graphql';
import { Dimension } from 'src/dimensions/entities/dimension.entity';

@InputType()
export class CreateProjectionInput {
  @Field(() => String, { description: 'name' })
  name!: String;

  @Field(() => [Dimension], { description: 'dimensions' })
  dimensions!: Dimension[];
}
