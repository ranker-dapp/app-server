import { Dimension } from 'src/dimensions/entities/dimension.entity';
import { CreateProjectionInput } from './create-projection.input';
import { InputType, Field, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateProjectionInput extends PartialType(CreateProjectionInput) {
  @Field(() => String, { description: 'current name' })
  currentName!: String;

  @Field(() => String, { description: 'name' })
  name!: String;

  @Field(() => [Dimension], { description: 'dimensions' })
  dimensions!: Dimension[];
}
