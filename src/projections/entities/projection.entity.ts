import { ObjectType, Field, InputType } from '@nestjs/graphql';
import { Dimension } from 'src/dimensions/entities/dimension.entity';

@InputType('ProjectionInput')
@ObjectType()
export class Projection {
  @Field(() => String, { description: 'name' })
  name!: String;

  @Field(() => Dimension, { description: 'average ranked dimension' })
  dimension?: Dimension;

  @Field(() => [Dimension], { description: 'dimensions' })
  dimensions!: Dimension[];
}
