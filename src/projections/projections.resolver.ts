import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { ProjectionsService } from './projections.service';
import { Projection } from './entities/projection.entity';
import { CreateProjectionInput } from './dto/create-projection.input';
import { UpdateProjectionInput } from './dto/update-projection.input';

@Resolver(() => Projection)
export class ProjectionsResolver {
  constructor(private readonly projectionsService: ProjectionsService) {}

  @Mutation(() => Projection)
  createProjection(@Args('createProjectionInput') createProjectionInput: CreateProjectionInput) {
    return this.projectionsService.create(createProjectionInput);
  }

  @Query(() => [Projection], { name: 'projections' })
  findAll() {
    return this.projectionsService.findAll();
  }

  @Query(() => Projection, { name: 'projection' })
  findOne(@Args('name', { type: () => String }) name: string) {
    const projection = this.projectionsService.findOne(name);
    return projection;
  }


  @Mutation(() => Projection)
  updateMurmuration(@Args('updateProjectionInput') { currentName, ...projection}: UpdateProjectionInput) {
    return this.projectionsService.update(currentName as string, projection as Projection);
  }

  @Mutation(() => Projection)
  removeProjection(@Args('name', { type: () => String }) name: string) {
    return this.projectionsService.remove(name);
  }
}
