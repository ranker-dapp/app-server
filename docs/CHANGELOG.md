# Ranker Server App Changelog

## [1.3.3](https://gitlab.com/ranker-dapp/app-server/compare/v1.3.2...v1.3.3) (11/16/2023)


### Bug Fixes

* **config:** use new configurer ([b53beb1](https://gitlab.com/ranker-dapp/app-server/commit/b53beb1250f7fcdf985e1b2b65854ee77cc5ade6))

## [1.3.2](https://gitlab.com/ranker-dapp/app-server/compare/v1.3.1...v1.3.2) (2023-11-15)


### Bug Fixes

* **build:** add apt upgrade ([bce0ef7](https://gitlab.com/ranker-dapp/app-server/commit/bce0ef73d5abb17751af5547ee7e758b5a6f7848))

## [1.3.1](https://gitlab.com/ranker-dapp/app-server/compare/v1.3.0...v1.3.1) (2023-11-10)


### Bug Fixes

* **deploy:** use dumb-init ([81b0502](https://gitlab.com/ranker-dapp/app-server/commit/81b0502d67977d8a67a7230c19f66f5b30d5774f))

# [1.3.0](https://gitlab.com/ranker-dapp/app-server/compare/v1.2.3...v1.3.0) (2023-11-08)


### Features

* **graphql:** enable introspection ([4c81b89](https://gitlab.com/ranker-dapp/app-server/commit/4c81b8901accb3f8bede165b5c845294cb7311cf))

## [1.2.3](https://gitlab.com/ranker-dapp/app-server/compare/v1.2.2...v1.2.3) (2023-11-08)


### Bug Fixes

* **ci:** retry dev image release ([6a55cb5](https://gitlab.com/ranker-dapp/app-server/commit/6a55cb599f3aa113e1984d9b24d1c620fe15db2b))

## [1.2.2](https://gitlab.com/ranker-dapp/app-server/compare/v1.2.1...v1.2.2) (2023-11-08)


### Bug Fixes

* **ci:** try -dev image name workflow ([2d9b2c3](https://gitlab.com/ranker-dapp/app-server/commit/2d9b2c3f9e8a4f4b8dab5c1ccf407e4e1e490932))

## [1.2.1](https://gitlab.com/ranker-dapp/app-server/compare/v1.2.0...v1.2.1) (2023-11-08)


### Bug Fixes

* **publish:** retry dev image release ([504e066](https://gitlab.com/ranker-dapp/app-server/commit/504e0667ef79b5e75a13695ec8577594ff7facd1))

# [1.2.0](https://gitlab.com/ranker-dapp/app-server/compare/v1.1.0...v1.2.0) (2023-11-08)


### Bug Fixes

* **release:** rework semantic release configuration ([19936c9](https://gitlab.com/ranker-dapp/app-server/commit/19936c999685a0fcf5ed285f74a9dfe06c57dfb2))


### Features

* **release:** publish both dev and live images ([3f8fb11](https://gitlab.com/ranker-dapp/app-server/commit/3f8fb11b3cd0ab300c498c21f104bb584fcb3f1d))

# [1.1.0](https://gitlab.com/ranker-dapp/app-server/compare/v1.0.0...v1.1.0) (2023-11-08)


### Features

* **config:** serve on regular http port 80 ([4c49470](https://gitlab.com/ranker-dapp/app-server/commit/4c49470e22be361e3e538c589f0a00c0d72abafe))

# 1.0.0 (2023-11-07)


### Bug Fixes

* **build:** use correct paths for multistage building ([563485f](https://gitlab.com/ranker-dapp/app-server/commit/563485f5ff51bdb97c0aa62784bcf36d2f1986c8))
* **dimensions:** remove lazy init ([d396e13](https://gitlab.com/ranker-dapp/app-server/commit/d396e1302e423f20430336bcbd4891535bb36c27))
* **items:** upsert on update ([f135b70](https://gitlab.com/ranker-dapp/app-server/commit/f135b707eab45bf3895aa99af9eaeed6efe5f16f))
* **registry:** use correct name ([b17ccb5](https://gitlab.com/ranker-dapp/app-server/commit/b17ccb5467c29b2ae724a56f425b638ab8d1c507))
* **registry:** use dns hostname and port ([021b5d6](https://gitlab.com/ranker-dapp/app-server/commit/021b5d6ea8f246a5194b73192f480a924ea4a7a2))
* **usage:** allow cors for more localhost ports ([b9c6053](https://gitlab.com/ranker-dapp/app-server/commit/b9c6053be14fc7779bff6c711d3455181aaecdda))


### Features

* **aggragations:** add murmurations and projections to aggregate ranks ([774725b](https://gitlab.com/ranker-dapp/app-server/commit/774725b8115bc7a7127843ad2edabd5070e46d96))
* **aggregates:** full merge items ([2c265a2](https://gitlab.com/ranker-dapp/app-server/commit/2c265a219d2ee91938dac8dfb682aace16a6188b))
* **deploy:** update deps and deploy docker image ([fd82054](https://gitlab.com/ranker-dapp/app-server/commit/fd82054d41bf0f8f634863ce523d9a99b45f3391))
* **dimension:** add dimensions resource ([0dcbc87](https://gitlab.com/ranker-dapp/app-server/commit/0dcbc87f8326e6ecfd36917a6ffafc72d6f2282e))
* **framework:** introduce nestjs ([5e203f2](https://gitlab.com/ranker-dapp/app-server/commit/5e203f2ece6d412caf4d87b9f78b6bd803d5e67c))
* **items:** add author to items ([dc2466c](https://gitlab.com/ranker-dapp/app-server/commit/dc2466c1dabcaafc48265f625facf4eb6fdff24d))
* **items:** add items ([1037b4f](https://gitlab.com/ranker-dapp/app-server/commit/1037b4fcc24802b456657f1c186a427f30834032))
* **ranking:** connect items to dimensions to users ([9505ce7](https://gitlab.com/ranker-dapp/app-server/commit/9505ce771632f20dd45c0e51d37fb133231da735))
* **release:** parameterize server port ([f58afe2](https://gitlab.com/ranker-dapp/app-server/commit/f58afe29a46c6772d9eb7f75d8b04403a55629c5))
* **security:** use scrypt ([00fa1c4](https://gitlab.com/ranker-dapp/app-server/commit/00fa1c435ae3e4bdb6662bb9f30cbc605ca6a042))
* **server:** add items mutation and query ([7542c0a](https://gitlab.com/ranker-dapp/app-server/commit/7542c0a00222c76dfe560e669aa5aa18170b5f01))
* **subscriptions:** add ws endpoint ([c9ff18f](https://gitlab.com/ranker-dapp/app-server/commit/c9ff18f43d0bc2fbaf881491e65183a2c0cbdc79))
* **subscriptions:** use newer graphql-ws server ([b13d067](https://gitlab.com/ranker-dapp/app-server/commit/b13d067bcc7803cd4110b5ec0742d0bada5524b0))
* **users:** add jwt authorization and users signup ([903f671](https://gitlab.com/ranker-dapp/app-server/commit/903f671db007ec18778fd16e2526ec1c0e043af4))
